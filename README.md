*CrowdBot Challenge v1 (paper version)*

This repository is a benchmark that allow you to test your mobile robot controller in dense crowds.
It is presented here: https://hal.archives-ouvertes.fr/hal-03206221.
It comes with a preset of scenarios which have all in common : a mobile robot, the turtlebot, which have the goal to reach the other side of a corridor. The corridor is full of moving people. The activity and density of the crowd vary according to various scenarios.
In this v1, you have access to the crowd state at any time step, you don't need to use sensors.

This v1 is using an application developped in Unity which render the simulation and uses sockets to communicate with a python script.
This python script allow you to get the crowd state and the robot state, and to send a new command to the robot for the next time step.
Using ROS is feasible and not mandatory.

Please leave an issue for any bug or question https://gitlab.inria.fr/CrowdBot/CrowdBotChallenge/-/issues.


**1. Set up**
-------------------------------------------

`git submodule update --init --recursive`

*- inside CB_challenge executable folder*

launch CB_challenge_...


*- inside crowdbotsimcontrol folder*

`pip install --user virtualenv`

`./install_dependencies.sh`

`source ~/cbcenv/bin/activate`

`python3 core.py`


**2. Setting the parameters**
-------------------------------------------

Open the core.py file inside crowdbotsimcontrol folder and modify the last line of the file according to the parameters you want:

`run(HOST='127.0.0.1', PORT=25001, time_step=0.1, sleep_time=-0.3, planner_type="DWA", crowd_is_reactive=True, crowd_simulator='ORCA1_5s')`

**HOST:** change to the address of the computer running the CB_challenge executable.

**PORT:** do not change this port

**time_step:** time between two steps of the simulation

**sleep_time:** actively wait this time, negative means real time simulation. Big simulations with a lot of agents need this to be bigger than time_step to be make sure unity finished to compute the current step. (0.3s is fine)

**planner_type:** robot navigation method, select "BASELINE" to go straight, "DWA" for dynamic window approach, and "RVO" for reciprocal velocity obstacle

**crowd_is_reactive:** wether or not the crowd reacts to the robot. Note: additionnal action are required to configure the crowd simulation, see below

**crowd_simulator:** the crowd simulation method, select 'ORCA1_5s', 'ORCA0_5s', or 'SocialForces'. Note: additionnal action are required to configure the crowd simulation, see below

**3. Configure the crowd reactivity**
-------------------------------------------

Inside the CB_challenge executable folder, the config.xml file refers to a list of scenarios.
To select the scenarios where the crowd is reactive, use the following version

`<sourceFileExperiment>../Scenarios/FileOrderReactive.csv</sourceFileExperiment>`

If you want to select the scenarios where the crowd is not reactive, use the following version:

`<sourceFileExperiment>../Scenarios/FileOrderNotReactive.csv</sourceFileExperiment>`

**4. Configure the crowd simulator**
-------------------------------------------

Inside the CB_challenger executable folder/UMANSFiles/examples/policies/, edit the default.xml file according to the following:

**ORCA1_5s :**
```
<Policy id="0" OptimizationMethod="global">
		<costfunction range="50" name="ORCA" timeHorizon="1.5"/>
</Policy>
```

**ORCA0_5s :** 
```
<Policy id="0" OptimizationMethod="global">
		<costfunction range="50" name="ORCA" timeHorizon="0.5"/>
</Policy>
```

**SocialForces :**
```
<Policy id="0" OptimizationMethod="global">
	<costfunction range="100" name="SocialForcesAvoidance" />
	<costfunction range="100" name="GoalReachingForce" />
</Policy>
```
