import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

class HistForm:
	def __init__(self, color, label, n_bins):
		self.color = color
		self.label = label
		self.bins = np.zeros([n_bins], dtype=int)

class HistMultiple:
	def __init__(self, x_lim, n_bins):
		self.x_lim = x_lim
		self.n_bins = n_bins
		self.bin_width = (x_lim[1] - x_lim[0])/n_bins
		self.histograms_forms = []

	def newHist(self, X, color, label):
		hist_form = HistForm(color, label, self.n_bins)
		X_sorted = np.sort(X)
		i = 0
		bin_lim_upper = self.x_lim[0]+self.bin_width
		for x in X_sorted:
			if x < self.x_lim[0]:
				continue
			while x >= bin_lim_upper:
				i += 1
				bin_lim_upper = self.x_lim[0]+self.bin_width*(i+1)
				if i == self.n_bins:
					break
			if i == self.n_bins:
				break
			hist_form.bins[i] += 1
		self.histograms_forms.append(hist_form)

	def plot(self, ax):
		for i in range(self.n_bins):
			c_max = 0
			for hist_form in self.histograms_forms:
				if hist_form.bins[i] > c_max:
					c_max = hist_form.bins[i]
			bin_lim_l = self.x_lim[0]+i*self.bin_width
			ax.plot([bin_lim_l, bin_lim_l], [0, c_max], "k")
			bin_lim_u = bin_lim_l + self.bin_width
			ax.plot([bin_lim_u, bin_lim_u], [0, c_max], "k")

		line_X = np.array([
			[self.x_lim[0]+(i+j)*self.bin_width for j in range(self.n_bins)]
			for i in range(2)
		])
		for hist_form in self.histograms_forms:
			line_Y = np.array([
				[hist_form.bins[j] for j in range(self.n_bins)]
				for i in range(2)
			])
			ax.plot(line_X, line_Y, hist_form.color, linewidth=2.5)
			ax.plot(line_X[:,0], line_Y[:,0], hist_form.color, linewidth=2.5, label=hist_form.label)
			for i in range(self.n_bins):
				bin_lim_l = self.x_lim[0]+i*self.bin_width
				rect = patches.Rectangle((bin_lim_l, 0),
					self.bin_width, hist_form.bins[i],
					edgecolor='none', facecolor=hist_form.color, alpha=0.4)
				ax.add_patch(rect)
			ax.set_xlim(self.x_lim)


def demo():
	X1 = np.random.normal(0.0, 1.0, size=[300])
	X2 = np.random.normal(2.0, 1.0, size=[250])
	X3 = np.random.normal(1.5, 0.5, size=[150])
	hm = HistMultiple([-2.0, 4.0], 30)
	hm.newHist(X1, "r")
	hm.newHist(X2, "g")
	hm.newHist(X3, "b")
	fig, ax = plt.subplots(1,1)
	hm.plot(ax)
	plt.show()

if __name__ == '__main__':
	demo()