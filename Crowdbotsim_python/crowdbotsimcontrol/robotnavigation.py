#!/usr/bin/env python3
import numpy as np
import random
import helpers
from rvo import NavigationPlanner

class baselinePlanner(NavigationPlanner):
    def __init__(self):
        pass
    
    def compute_cmd_vel(self, crowd, robot_pose, goal, show_plot=True, debug=False):
        speed = 1.0
        rot = 0.0
        return (speed, rot)
