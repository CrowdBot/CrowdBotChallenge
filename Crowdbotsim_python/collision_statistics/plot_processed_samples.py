import matplotlib.pyplot as plt
import cPickle as pickle
from multiple_hist import HistMultiple
import numpy as np

def load_object(filename):
	with open(filename, 'rb') as input:
		return pickle.load(input)

samples_per_method_and_body_part_ORCA0_5 = load_object("samplesORCA0_5s.pkl")
samples_per_method_and_body_part_ORCA1_5 = load_object("samplesORCA1_5s.pkl")
samples_per_method_and_body_part_SocialForces = load_object("samplesSocialForces.pkl")
samples_per_cs_tech = [samples_per_method_and_body_part_ORCA0_5,
	samples_per_method_and_body_part_ORCA1_5, samples_per_method_and_body_part_SocialForces]
cs_titles = ["ORCA0_5s", "ORCA1_5s", "SocialForces"]
body_parts_keys = ["Toe0", "Calf", "Thigh", "UpperArm"]
reflected_masses = [4.0, 12.5, 23.89, 1.0]
m_robot = 20.0
mus = []
for m_refl in reflected_masses:
	mus.append(m_robot*m_refl/(m_robot+m_refl))

#Total simulation times: Baseline, DWA, RVO
cum_sim_times = [6606.7, 11545.3, 12663.9]
n_energy_outliers = 0
n_energy_inliers = 0

for metric in ["speed", "kinetic_energy"]:
	if metric == "kinetic_energy":
		for s in range(len(samples_per_cs_tech)):
			for m in range(len(samples_per_cs_tech[s])):
				for b in range(len(samples_per_cs_tech[s][m])):
					for i in range(len(samples_per_cs_tech[s][m][b])):
						impact_speed = samples_per_cs_tech[s][m][b][i]
						mu = mus[b]
						kinetic_energy = 0.5*mu*impact_speed*impact_speed
						samples_per_cs_tech[s][m][b][i] = kinetic_energy

	accumulation = np.zeros([3, 3])
	if not metric == "kinetic_energy":
		accumulation = np.zeros([3, 3], dtype=int)
	for s in range(len(samples_per_cs_tech)):
		for m in range(len(samples_per_cs_tech[s])):
			for b in range(len(samples_per_cs_tech[s][m])):
				for i in range(len(samples_per_cs_tech[s][m][b])):
					if metric == "kinetic_energy":
						if samples_per_cs_tech[s][m][b][i] > 50.0:
							n_energy_outliers += 1
							continue
						n_energy_inliers += 1
						accumulation[s, m] += samples_per_cs_tech[s][m][b][i]
					else:
						accumulation[s, m] += 1
	if not metric == "kinetic_energy":
		print "Number of collisions (Baseline | DWA | RVO )"
		for s in range(len(samples_per_cs_tech)):
			print "%-15s: %15i | %15i | %15i" % (cs_titles[s],
				accumulation[s, 0], accumulation[s, 1], accumulation[s, 2])
		print "%-15s: %15i | %15i | %15i" % ("Sum overall",
			np.sum(accumulation[:, 0]), np.sum(accumulation[:, 1]), np.sum(accumulation[:, 2]))
		print "%-15s: %15.3f | %15.3f | %15.3f" % ("Rate [1/s]",
			np.sum(accumulation[:, 0])/cum_sim_times[0], np.sum(accumulation[:, 1])/cum_sim_times[1], np.sum(accumulation[:, 2])/cum_sim_times[2])
	else:
		print "Kinetic energy into collisions [J] (Baseline | DWA | RVO )"
		for s in range(len(samples_per_cs_tech)):
			print "%-15s: %15.0f | %15.0f | %15.0f" % (cs_titles[s],
				accumulation[s, 0], accumulation[s, 1], accumulation[s, 2])
		print "%-15s: %15.0f | %15.0f | %15.0f" % ("Sum overall",
			np.sum(accumulation[:, 0]), np.sum(accumulation[:, 1]), np.sum(accumulation[:, 2]))
		print  "%-15s: %15.3f | %15.3f | %15.3f" % ("Rate [J/s]",
			np.sum(accumulation[:, 0])/cum_sim_times[0], np.sum(accumulation[:, 1])/cum_sim_times[1], np.sum(accumulation[:, 2])/cum_sim_times[2])
		print ("n_energy_outliers", n_energy_outliers)
		print ("n_energy_inliers", n_energy_inliers)

	fig, axs = plt.subplots(4,4, figsize=(11,9))

	hm_xlim = [0.0, 2.5]
	if metric == "kinetic_energy" and m_robot > 1.0:
		hm_xlim = [0.0, 30.0]

	for b in range(3):
		for s in range(3):
			ax = axs[b][s]
			X1 = samples_per_cs_tech[s][0][b]
			X2 = samples_per_cs_tech[s][1][b]
			X3 = samples_per_cs_tech[s][2][b]
			hm = HistMultiple(hm_xlim, 12)
			hm.newHist(X1, "b", "Baseline")
			hm.newHist(X2, "g", "DWA")
			hm.newHist(X3, "r", "RVO")
			hm.plot(ax)
			if b == 0 and s == 0:
				ax.legend(bbox_to_anchor=(-0.35, 1))
			if s == 0:
				ax.set_ylabel(body_parts_keys[b], rotation=90,
					size='large')
			if b == 0:
				ax.set_title(cs_titles[s])

	b = 3
	for s in range(3):
		ax = axs[b][s]
		X1 = []
		X2 = []
		X3 = []
		for i in range(3):
			X1 = X1+samples_per_cs_tech[s][0][i]
			X2 = X2+samples_per_cs_tech[s][1][i]
			X3 = X3+samples_per_cs_tech[s][2][i]
		hm = HistMultiple(hm_xlim, 12)
		hm.newHist(X1, "b", "Baseline")
		hm.newHist(X2, "g", "DWA")
		hm.newHist(X3, "r", "RVO")
		hm.plot(ax)
		if s == 0:
			if not metric == "kinetic_energy":
				ax.set_xlabel("impact speed [m/s]", size='large')
			else:
				ax.set_xlabel("impact energy [J]", size='large')
			ax.set_ylabel("Toe+Calf+Thigh", size="large")


	s = 3
	for b in range(3):
		ax = axs[b][s]
		X1 = []
		X2 = []
		X3 = []
		for i in range(3):
			X1 = X1+samples_per_cs_tech[i][0][b]
			X2 = X2+samples_per_cs_tech[i][1][b]
			X3 = X3+samples_per_cs_tech[i][2][b]
		hm = HistMultiple(hm_xlim, 12)
		hm.newHist(X1, "b", "Baseline")
		hm.newHist(X2, "g", "DWA")
		hm.newHist(X3, "r", "RVO")
		hm.plot(ax)
		if b == 0:
			ax.set_title("ORCA0_5s+ORCA1_5s+\nSocialForces")

	b = 3
	s = 3
	ax = axs[b][s]
	X1 = []
	X2 = []
	X3 = []
	for i in range(3):
		for j in range(3):
			X1 = X1+samples_per_cs_tech[i][0][j]
			X2 = X2+samples_per_cs_tech[i][1][j]
			X3 = X3+samples_per_cs_tech[i][2][j]
	hm = HistMultiple(hm_xlim, 12)
	hm.newHist(X1, "b", "Baseline")
	hm.newHist(X2, "g", "DWA")
	hm.newHist(X3, "r", "RVO")
	hm.plot(ax)

	plt.savefig(metric+'_hist_joint_marginal.png', bbox_inches='tight', dpi=399)
	plt.show()

	if not metric == "kinetic_energy":
		continue
	fig, ax = plt.subplots(1,1, figsize=(6,5))
	X1 = []
	X2 = []
	X3 = []
	for i in range(3):
		for j in range(3):
			X1 = X1+samples_per_cs_tech[i][0][j]
			X2 = X2+samples_per_cs_tech[i][1][j]
			X3 = X3+samples_per_cs_tech[i][2][j]
	hm = HistMultiple([0.0, 25.0], 28)
	hm.newHist(X1, "b", "Baseline")
	hm.newHist(X2, "g", "DWA")
	hm.newHist(X3, "r", "RVO")
	hm.plot(ax)
	handles, labels = ax.get_legend_handles_labels()
	order = [1, 0, 2]
	ax.legend([handles[idx] for idx in order], [labels[idx] for idx in order])
	ax.set_xlabel("impact energy [J]", size='large')
	ax.set_ylabel("number of collisions [-]", size='large')

	plt.savefig(metric+'_hist_all.png', bbox_inches='tight', dpi=399)
	plt.show()

quit()

for s in range(3):
	samples_per_method_and_body_part = samples_per_cs_tech[s]

	body_parts_keys = ["Toe0", "Calf", "Thigh", "UpperArm"]
	controller_names = ["Baseline", "DWA", "RVO"]
	fig, axs = plt.subplots(3, 3, figsize=(8,6))#4)



			#axs[m][i].locator_params(axis='x', nbins=4)
	#fig.tight_layout()
	#plt.boxplot(body_parts_samples, 1, 'x')
	#plt.savefig('all_histograms.png', bbox_inches='tight', dpi=199)
	plt.show()

	X1 = []
	X2 = []
	X3 = []
	for i in range(3):
		X1 = X1+samples_per_method_and_body_part[0][i]
		X2 = X2+samples_per_method_and_body_part[1][i]
		X3 = X3+samples_per_method_and_body_part[2][i]
	hm = HistMultiple([0.0, 2.0], 12)
	hm.newHist(X1, "b", "Baseline")
	hm.newHist(X2, "g", "DWA")
	hm.newHist(X3, "r", "RVO")
	fig, ax = plt.subplots(1,1)
	hm.plot(ax)
	ax.set_xlabel("impact speed [m/s]", size='large')
	ax.set_ylabel("number of collisions", size='large')
	ax.legend()
	plt.savefig('multi_hist.png', bbox_inches='tight', dpi=199)
	plt.show()