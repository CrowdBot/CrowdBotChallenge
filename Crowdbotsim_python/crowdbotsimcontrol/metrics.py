#!/usr/bin/env python3
import recorder
import helpers
import numpy as np
import matplotlib.pyplot as plt
import sys
np.set_printoptions(threshold=sys.maxsize)

def eucl_dist(a,b):
    return np.linalg.norm(np.array(b)-np.array(a))

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    if np.linalg.norm(vector) == 0:
        return 0
    else:
        return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    if np.linalg.norm(v1) == 0 or np.linalg.norm(v2) == 0:
        return 0
    else :
        return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def compute_path_efficiency(alone_json, crowded_json):
    #T alone / T crowded
    #L alone / L crowded
    #J alone / J crowded

    # print(crowded_json)
    # print('path efficiency')

    alone = recorder.load_file_as_list_of_dico(alone_json)
    crowded = recorder.load_file_as_list_of_dico(crowded_json)
    
    alone_odom_xy = fill_empty_sets([ helpers.get_odom(alone_i)[:2] for i,alone_i in enumerate(alone) ])
    crowded_odom_xy = fill_empty_sets([ helpers.get_odom(crowded_i)[:2] for i,crowded_i in enumerate(crowded) ])

    #filter_out bad begining
    for start_index, val in enumerate(crowded_odom_xy):
        if val[0] > 0:
            break 

    crowded = crowded[start_index:]
    crowded_odom_xy = crowded_odom_xy[start_index:]

    Talone = float(alone[-1]['clock']) - float(alone[0]['clock'])
    Tcrowded = float(crowded[-1]['clock']) - float(crowded[0]['clock'])

    Lalone = sum( [ eucl_dist(alone_odom_xy[i], alone_odom_xy[i+1]) for i in range(len(alone_odom_xy)-1) ] )
    Lcrowded = sum( [ eucl_dist(crowded_odom_xy[i], crowded_odom_xy[i+1]) for i in range(len(crowded_odom_xy)-1) ] )

    alone_odom = fill_empty_sets([ helpers.get_odom(alone_i)[:3] for i,alone_i in enumerate(alone) ])
    crowded_odom = fill_empty_sets([ helpers.get_odom(crowded_i)[:3] for i,crowded_i in enumerate(crowded) ])

    Jalone = np.sum(np.abs(np.diff(np.diff(np.reshape(np.ravel(alone_odom, order='F'),(3,-1))))))

    Jcrowded = np.sum(np.abs(np.diff(np.diff(np.reshape(np.ravel(crowded_odom, order='F'),(3,-1))))))
    
    return (min(1,Talone/Tcrowded), min(1,Lalone/Lcrowded), min(1,Jalone/Jcrowded))

def compute_effect_on_crowd(not_reactive_json, reactive_json):
    # print("effect")

    # not_reactive_crowd = recorder.load_file_as_list_of_dico(not_reactive_json)
    reactive_crowd = recorder.load_file_as_list_of_dico(reactive_json)

    # nr_odom_xy = fill_empty_sets([ helpers.get_odom(nr_i)[:2] for i,nr_i in enumerate(not_reactive_crowd) ])
    r_odom_xy = fill_empty_sets([ helpers.get_odom(r_i)[:2] for i,r_i in enumerate(reactive_crowd) ])

    # #filter_out bad begining
    # for start_index_nr, val in enumerate(nr_odom_xy):
    #     if val[0] > 0:
    #         break 

    # not_reactive_crowd = not_reactive_crowd[start_index_nr:]

    #filter_out bad begining
    for start_index_r, val in enumerate(r_odom_xy):
        if val[0] > 0:
            break 

    reactive_crowd = reactive_crowd[start_index_r:]

    # Tcrowded = ??
    # Tcrowd_alone = ??

    dt = float(reactive_crowd[1]['clock']) - float(reactive_crowd[0]['clock'])
    
    # nr_crowd = fill_empty_sets([ helpers.get_crowd(not_reactive_crowd_i) for i,not_reactive_crowd_i in enumerate(not_reactive_crowd) ])
    # nr_crowd_traj = np.array([  [ nr_crowd[i][j][1:] for i in range(len(nr_crowd)) ] for j in range(len(nr_crowd[0])) ])
    # nr_crowd_dist = np.array([ [ eucl_dist(nr_crowd_traj[i][j], nr_crowd_traj[i][j+1]) for j in range(len(nr_crowd_traj[i])-1)] for i in range(len(nr_crowd_traj))])
    # nr_crowd_vel = np.where( np.abs(nr_crowd_dist) < 5, nr_crowd_dist, 0)/dt

    # Vel_not_reactive_crowd = np.mean(nr_crowd_vel)

    r_crowd = fill_empty_sets([ helpers.get_crowd(reactive_crowd_i) for i,reactive_crowd_i in enumerate(reactive_crowd) ])
    r_crowd_traj = np.array([  [ r_crowd[i][j][1:] for i in range(len(r_crowd)) ] for j in range(len(r_crowd[0])) ])
    r_crowd_dist = np.array([ [ eucl_dist(r_crowd_traj[i][j], r_crowd_traj[i][j+1]) for j in range(len(r_crowd_traj[i])-1)] for i in range(len(r_crowd_traj))])
    r_crowd_vel = np.where( np.abs(r_crowd_dist) < 5, r_crowd_dist, 0)/dt

    Vel_reactive_crowd = np.mean(r_crowd_vel)

    r_crowd_angle = np.array([ [ angle_between(r_crowd_traj[i][j+1] - r_crowd_traj[i][j], r_crowd_traj[i][j+2] - r_crowd_traj[i][j+1]) for j in range(len(r_crowd_traj[i])-2)] for i in range(len(r_crowd_traj))])
    r_crowd_ang_vel = np.abs(r_crowd_angle) / dt

    ang_Vel_reactive_crowd = np.mean(r_crowd_ang_vel)

    #Neighbors

    neighbors_distance = 2
    Vel_neighbors = np.array([])

    reactive_odom_xy = fill_empty_sets([ helpers.get_odom(reac_i)[:2] for i,reac_i in enumerate(reactive_crowd) ])

    is_near = np.array([[eucl_dist(reactive_odom_xy[j], r_crowd_traj[i][j]) < 1 for j in range(len(r_crowd_traj[i])-1)] for i in range(len(r_crowd_traj))])

    neighbor_vel = np.array([])
    for top_index,top_val in enumerate(r_crowd_vel):
        for index,val in enumerate(top_val):
            if is_near[top_index][index]:
                neighbor_vel = np.append(neighbor_vel, val )
    
    Vel_reactive_neighbors = np.mean(neighbor_vel)

    neighbor_ang_vel = np.array([])
    for top_index,top_val in enumerate(r_crowd_ang_vel):
        for index,val in enumerate(top_val):
            if is_near[top_index][index]:
                neighbor_ang_vel = np.append(neighbor_ang_vel, val )
    
    ang_Vel_reactive_neighbors = np.mean(neighbor_ang_vel)

    # return (Vel_not_reactive_crowd/Vel_reactive_crowd, Vel_reactive_neighbors/Vel_reactive_crowd)
    return (ang_Vel_reactive_crowd/ang_Vel_reactive_neighbors, Vel_reactive_neighbors/Vel_reactive_crowd)


def compute_crowd_robot_interaction(scenario_json):
    # print('proximity ' + scenario_json)
    #proximity 1
    scenario = recorder.load_file_as_list_of_dico(scenario_json)

    odom_xy = fill_empty_sets([ helpers.get_odom(sc_i)[:2] for i,sc_i in enumerate(scenario) ])

    #filter_out bad begining
    for start_index, val in enumerate(odom_xy):
        if val[0] > 0:
            break 

    scenario = scenario[start_index:]

    total_time = float(scenario[-1]['clock']) - float(scenario[0]['clock'])

    odom_xy = odom_xy[start_index:]

    crowd = fill_empty_sets([ helpers.get_crowd(crowd_i) for i,crowd_i in enumerate(scenario) ])

    crowd_xy = np.array([  [ crowd[i][j][1:] for i in range(len(crowd)) ] for j in range(len(crowd[0])) ])

    relative_distances = np.array([[eucl_dist(odom_xy[j], crowd_xy[i][j]) for j in range(len(crowd_xy[i])-1)] for i in range(len(crowd_xy))])

    Proximity = np.array([])

    for step in range(len(relative_distances[0])): #time values
        values_at_this_step = np.array([])
        for id_index, time_values in enumerate(relative_distances):
            values_at_this_step = np.append(values_at_this_step, time_values[step])
        
    Proximity = np.append(Proximity, np.amin( values_at_this_step )/5.0)

    return 1 - min(np.mean(Proximity),1)

def get_time_spent_colliding(scenario_json):
    scenario = recorder.load_file_as_list_of_dico(scenario_json)

    odom_xy = fill_empty_sets([ helpers.get_odom(sc_i)[:2] for i,sc_i in enumerate(scenario) ])

    #filter_out bad begining
    for start_index, val in enumerate(odom_xy):
        if val[0] > 0:
            break 

    scenario = scenario[start_index:]
    odom_xy = odom_xy[start_index:]

    colliding = np.array([ sc_i["report"] != "" for i,sc_i in enumerate(scenario) ])
    clock = np.array([ float(sc_i["clock"]) for i,sc_i in enumerate(scenario) ])

    time_colliding = np.sum(np.where(colliding, clock[1]-clock[0], 0))
    total_time = float(scenario[-1]['clock']) - float(scenario[0]['clock'])

    return min(1,1-(time_colliding/total_time))

def make_radar_chart(name, stats, attribute_labels, plot_markers, plot_str_markers):

    # name = name + " low density"
    plt.rc('font', size=15)          
    plt.rc('ytick', labelsize=12)         

    labels = np.array(attribute_labels)

    angles = np.linspace(0, 2*np.pi, len(labels), endpoint=False)
    stats = np.concatenate((stats,[stats[0]]))
    angles = np.concatenate((angles,[angles[0]]))

    fig= plt.figure()
    ax = fig.add_subplot(111, polar=True)
    ax.plot(angles, stats, '-', linewidth=2)
    ax.fill(angles, stats, alpha=0.25)

    for label, angle in zip(ax.get_xticklabels(), angles):
        if np.pi/2 < angle < 3*np.pi/2:
            label.set_horizontalalignment('right')
        else:
            label.set_horizontalalignment('left')

    ax.set_thetagrids(angles[:-1] * 180/np.pi, labels)
    ax.set_ylim(0, 1)
    plt.yticks(plot_markers)
    ax.set_title(name)
    ax.grid(True)

    fig.savefig("images/tests/%s.png" % name)

    return
    return plt.show()

def make_radar_chart2(name, dico, attribute_labels, plot_markers, plot_str_markers):

    plt.rc('font', size=15)          
    plt.rc('ytick', labelsize=12)         

    labels = np.array(attribute_labels)

    angles = np.linspace(0, 2*np.pi, len(labels), endpoint=False)
    angles = np.concatenate((angles,[angles[0]]))

    fig= plt.figure(figsize=(10,7))
    ax = fig.add_subplot(111, polar=True)
    
    # Helper function to plot each car on the radar chart.
    def add_to_radar(robot_nav, color):
        values = dico[robot_nav]
        values += values[:1]
        ax.plot(angles, values, color=color, linewidth=2, label=robot_nav)
        ax.fill(angles, values, color=color, alpha=0.25)
        # ax.plot(angles, stats, '-', linewidth=2)
        # ax.fill(angles, stats, alpha=0.25)

    colors = {
        'BASELINE' : 'b',
        'DWA' : 'g',
        'RVO' : 'r'
    }
    for key, value in dico.items():
        add_to_radar(key, colors[key])

    for label, angle in zip(ax.get_xticklabels(), angles):
        if np.pi/2 < angle < 3*np.pi/2:
            label.set_horizontalalignment('right')
        else:
            label.set_horizontalalignment('left')

    ax.set_thetagrids(angles[:-1] * 180/np.pi, labels)
    ax.set_ylim(0, 1)
    plt.yticks(plot_markers)
    ax.set_title(name)
    # Add a legend as well.
    ax.legend(loc='upper right', bbox_to_anchor=(1.4, 1.1))
    ax.grid(True)

    fig.savefig("images/tests/%s.png" % name)

    return plt.show()

def fill_empty_sets(my_set):
    #fill empty sets
    base_shape = my_set[2].shape
    for i, val in enumerate(my_set):
        if val.shape != base_shape:
            try:
                if my_set[i+1].shape==my_set[i-1].shape:
                    my_set[i] = np.mean((my_set[i-1], my_set[i+1]), axis=0)
                else:
                    my_set[i] = np.mean((my_set[i-2], my_set[i+2]), axis=0)
            except IndexError:
                my_set[0] = my_set[1]
                pass

    return np.array(my_set)
