import json
import numpy as np
import matplotlib.pyplot as plt

class SampleCollector:
	def __init__(self, filepath, no_processing=False):
		self.body_parts = [
			"Toe0", "Calf", "Thigh", "UpperArm"
		]
		self.form = {
			"Toe0" : [[[], -2] for i in range(0,400)],# [[[], -2]]*400,
			"Calf" : [[[], -2] for i in range(0,400)],#[[[], -2]]*400,
			"Thigh" : [[[], -2] for i in range(0,400)],#[[[], -2]]*400,
			"UpperArm" : [[[], -2] for i in range(0,400)],#[[[], -2]]*400,
		}
		with open(filepath) as f:
			self.json_data = json.load(f)
		if not no_processing:
			self.fillForm()

	def plotForm(self):
		fig, axs = plt.subplots(1,4)
		for k in range(4):
			for i in range(len(self.form[self.body_parts[k]])):
				axs[k].scatter([i]*len(self.form[self.body_parts[k]][i][0]),
					self.form[self.body_parts[k]][i][0])
		plt.show()

	def fillForm(self):
		for j in range(len(self.json_data)):
			crowd_words = self.json_data[j]["crowd"][7:].split()
			ID = np.zeros([len(crowd_words)/3], dtype=int)
			XY = np.zeros([len(crowd_words)/3, 2], dtype=float)
			for l in range(len(crowd_words)/3):
				for m in range(3):
					word = crowd_words[l*3 + m]
					if m == 0:
						ID[l] = int(word)
					elif m == 1:
						XY[l, 0] = float(word[1:])
					else:
						XY[l, 1] = float(word[:-1])

			odom_words = self.json_data[j]["odom"][4:].split()
			if len(odom_words) < 2:
				continue
			xy_r = np.array([float(odom_words[0]), float(odom_words[1])])
			
			report_words = self.json_data[j]["report"].replace('|', ' ').split()
			#print (j)
			for i in range(len(report_words)):
				if (report_words[i] == "(L") or (report_words[i] == "(R"):
					colliding_agent_id = int(report_words[i-1])
					name = report_words[i+1]
					rel_velocity = None
					for k in range(i+2, len(report_words)):
						if report_words[k][0] == "(" and not ((report_words[k] == "(L") or (report_words[k] == "(R")):
							rel_velocity = np.array(
								[float(report_words[k][1:-1]),
								-float(report_words[k+2][:-1])])
							break
					if rel_velocity == None:
						continue

					if self.form[name][colliding_agent_id][1] == j:
						continue
					elif self.form[name][colliding_agent_id][1] == j-1:
						self.form[name][colliding_agent_id][1] = j
						continue
					
					colliding_agent_xy = None
					for k in range(ID.shape[0]):
						if ID[k] == colliding_agent_id:
							colliding_agent_xy = XY[k, :]
							break
					if colliding_agent_xy == None:
						continue
					pos_diff = colliding_agent_xy - xy_r
					distance = np.sqrt(pos_diff[0]*pos_diff[0]+pos_diff[1]*pos_diff[1])
					if distance < 0.01:
						continue
					normal = pos_diff/distance
					speed = np.abs(np.sum(rel_velocity*normal))
					self.form[name][colliding_agent_id][0].append(speed)
					self.form[name][colliding_agent_id][1] = j
			
class RecordParser:
	def __init__(self, filepath):
		with open(filepath) as f:
			self.json_data = json.load(f)
		self.record = [None]*len(self.json_data)

	def collectNames(self):
		names = []
		for i in range(len(self.json_data)):
			new_names = self.collectNamesFromEntry(self.json_data[i])
			if not new_names == None:
				names = names + new_names
		return list(set(names))

	def collectNamesFromEntry(self, json_data_entry):
		report_words = json_data_entry["report"].replace('|', ' ').split()
		if len(report_words) == 0:
			return None

		names = []
		ids = []
		for i in range(len(report_words)):
			if (report_words[i] == "(L") or (report_words[i] == "(R"):
				ids.append(report_words[i-1])
				names.append(report_words[i+1])
		return list(set(names + ids))

		for j in range(len(report_words)):
			report_words[j] = ''.join(i for i in report_words[j] if not i.isdigit())
		return list(set(report_words))
		names = []
		switcher = 0
		prefix = ""
		for word in report_words:
			if switcher == 0:
				switcher += 1
			elif switcher == 1:
				if word == "(R":
					prefix = "R_"
				elif word == "(L":
					prefix = "L_"
				else:
					names.append(prefix + word)
					prefix = ""
					switcher += 1
			elif switcher == 2:
				switcher += 1
			elif switcher == 3:
				switcher += 1
			elif switcher == 4:
				switcher += 1
			elif switcher == 5:
				switcher = 0
		return list(set(names))

	def formatJsonData(self):
		for i in range(len(self.json_data)):
			self.record[i] = self.formatEntry(self.json_data[i])

	def formatEntry(self, json_data_entry):
		crowd_words = json_data_entry["crowd"][7:].split()
		ID = np.zeros([len(crowd_words)/3], dtype=int)
		XY = np.zeros([len(crowd_words)/3, 2], dtype=float)
		for l in range(len(crowd_words)/3):
			for m in range(3):
				word = crowd_words[l*3 + m]
				if m == 0:
					ID[l] = int(word)
				elif m == 1:
					XY[l, 0] = float(word[1:])
				else:
					XY[l, 1] = float(word[:-1])

		odom_words = json_data_entry["odom"][4:].split()
		xy_r = np.array([float(odom_words[0]), float(odom_words[1])])

		report_words = json_data_entry["report"].replace('|', ' ').split()
		if len(report_words) == 0:
			return None
		collisions = []
		switcher = 0
		prefix = ""
		for word in report_words:
			if switcher == 0:
				collisions.append([word])
				switcher += 1
			elif switcher == 1:
				if word == "(R":
					prefix = "R_"
				elif word == "(L":
					prefix = "L_"
				else:
					collisions[-1][0] = prefix + word + " " + collisions[-1][0]
					prefix = ""
					switcher += 1
			elif switcher == 2:
				switcher += 1
			elif switcher == 3:
				collisions[-1].append(np.array([float(word[1:-1]), 0.0]))
				switcher += 1
			elif switcher == 4:
				switcher += 1
			elif switcher == 5:
				collisions[-1][-1][1] = -float(word[:-1])
				switcher = 0