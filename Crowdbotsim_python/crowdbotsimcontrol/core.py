#!/usr/bin/env python3
from signal import signal, SIGINT
from sys import exit, argv
import time
import numpy as np
from timeit import default_timer as timer

import helpers
import socket_handler
import robotnavigation
import recorder
import metrics
from rvo import RVONavigationPlanner
from dwa import DynamicWindowApproachNavigationPlanner


# @Daniel: main function, if you have trouble with real time simulation, you can set time_step and sleep_time to bigger values
def run(HOST='127.0.0.1', PORT=25001, time_step=0.1, sleep_time=0.3, planner_type="DWA", crowd_is_reactive=True, crowd_simulator='ORCA1_5s'):
    s = socket_handler.init_socket(HOST, PORT)

    def handler(signal_received, frame):
        # Handle any cleanup here
        print('SIGINT or CTRL-C detected. Exiting gracefully')
        socket_handler.stop(s)
        exit(0)
    signal(SIGINT, handler)

    print("Running simulation")

    # 21 scenarios
    for i in range(21):
        list_save = []
        print("Scenario # " + str(i))
        pub = {'clock': 0, 'vel_cmd': (0, 0), 'sim_control': 'i'}

        # send a few packet to be sure it is launched
        for _ in range(5):
            to_send = helpers.publish_all(pub)
            raw = socket_handler.send_and_receive(s, to_send)
            pub = helpers.do_step(time_step, pub)
            time.sleep(time_step*5)

        if planner_type == "RVO":
            planner = RVONavigationPlanner()
        elif planner_type == "DWA":
            planner = DynamicWindowApproachNavigationPlanner()
        elif planner_type == "BASELINE":
            planner = robotnavigation.baselinePlanner()
        else:
            raise NotImplementedError

        planner.set_static_obstacles(helpers.get_cb_challenge_static_obstacles())

        last_cmd_vel = (0, 0)
        while True:
            time_in = time.time()
            # making the raw string to send from the dict
            to_send = helpers.publish_all(pub)
            # sending and receiving raw data
            raw = socket_handler.send_and_receive(s, to_send)
            # getting dict from raw data
            dico = helpers.raw_data_to_dict(raw)

            # do cool stuff here
            to_save = {k: dico[k] for k in ('clock', 'crowd', 'odom', 'report')}
            list_save.append(to_save)

            # @Fabien: how do I get the true goal? => cross the line defined as x=-20 (robot start at 20)
            goal = np.array(helpers.get_odom(dico)[:2])
            try:
               goal[0] = -20.0
            except (ValueError, IndexError):
               pub = helpers.do_step(0, pub)
               continue

            # goal = [-20.0, 0.0]

            # @Fabien: how do I get crowd velocities?
            crowd = helpers.get_crowd(dico)

            # @Fabien: odom velocities are 0, should be higher
            # I boosted the max acc of the robot
            # the velocity drop to zero instantly during collision
            odom = helpers.get_odom(dico)

            try:
                x, y, th, _, _, _ = odom
            except ValueError:
               pub = helpers.do_step(0, pub)
               continue
 

            speed, rot = last_cmd_vel
            odom[3] = speed * np.cos(th)
            odom[4] = speed * np.sin(th)
            odom[5] = rot
            
            tic = timer()
            cmd_vel = planner.compute_cmd_vel(
                crowd,
                odom,
                goal,
                show_plot=False,
            )
            toc = timer()
            # print("{}Hz".format(1./(toc-tic)))

            last_cmd_vel = cmd_vel

            speed, rot = cmd_vel

            pub['vel_cmd'] = (speed, np.rad2deg(rot))

            # checking ending conditions
            if helpers.check_ending_conditions(180.0, -20, dico):
                break

            # if helpers.get_odom(dico)[0] <= goal[0]:
            #     break

            # Debug: This skips the first test, remove
            # if i != 20 :
            #     break

            # doing a step
            pub = helpers.do_step(time_step, pub)

            time_out = time.time()

            if sleep_time > 0:
                time.sleep(sleep_time)
            elif time_out < time_in + time_step:
                time.sleep(time_in + time_step - time_out)

        # next scenario!
        socket_handler.send_and_receive(s, helpers.publish_all(helpers.next()))

        # wait for sim to load new scenario
        time.sleep(1)

        if crowd_is_reactive:
            reactivity = '_reactive_'
        else:
            reactivity = '_notreactive_'
        
        # if i == 20:
        #     recorder.save_dico('./recorder/'+crowd_simulator+'/'+planner_type+reactivity+str(i), list_save)

    socket_handler.stop(s)
    time.sleep(1)

def demo_run(HOST='127.0.0.1', PORT=25001, time_step=0.05):
    s = socket_handler.init_socket(HOST, PORT)

    def handler(signal_received, frame):
        # Handle any cleanup here
        print('SIGINT or CTRL-C detected. Exiting gracefully')
        socket_handler.stop(s)
        exit(0)
    signal(SIGINT, handler)

    print("Running simulation")

    pub = {'clock': 0, 'vel_cmd_pepper': (0, 0), 'vel_cmd_qolo': (0, 0), 'vel_cmd_turtlebot': (
        0, 0), 'vel_cmd_cuybot': (0, 0), 'vel_cmd_wheelchair': (0, 0), 'sim_control': 'i'}
    while True:
        time_in = time.time()
        # making the raw string to send from the dict
        to_send = helpers.publish_all(pub)

        # sending and receiving raw data
        raw = socket_handler.send_and_receive(s, to_send)

        # getting dict from raw data
        dico = helpers.raw_data_to_dict(raw)

        # do cool stuff here
        # recorder.save_dico('recorder/test_'+str(i),dico)

        # pub['vel_cmd_pepper'] = robotnavigation.compute_rand_vel_cmd(dico)
        # pub['vel_cmd_qolo'] = robotnavigation.compute_rand_vel_cmd(dico)
        # pub['vel_cmd_turtlebot'] = robotnavigation.compute_rand_vel_cmd(dico)
        # pub['vel_cmd_cuybot'] = robotnavigation.compute_rand_vel_cmd(dico)
        # pub['vel_cmd_wheelchair'] = robotnavigation.compute_rand_vel_cmd(dico)

        # doing a step
        pub = helpers.do_step(time_step, pub)

        time_out = time.time()

        if time_out < time_in + time_step:
            time.sleep(time_in + time_step - time_out)

        time.sleep(1.5)

    socket_handler.stop(s)

def compute_metrics():
    relative_path = '../crowdbotsimcontrol/'
    directories = [relative_path+'recorder/ORCA0_5s/', relative_path+'recorder/ORCA1_5s/', relative_path+'recorder/SocialForces/']
    # directories = [relative_path+'recorder/SocialForces/']
    robot_nav = ['BASELINE', 'DWA', 'RVO']
    # robot_nav = ['DWA', 'BASELINE']
    # robot_nav = ['BASELINE']
    reactivity = ['_reactive_','_notreactive_']
    extension = '.json'

    #plot
    # labels=['Talone/Tcrowded' , 'Lalone/Lcrowded', 'Jalone/Jcrowded', 'Vel_not_reactive_crowd/Vel_reactive_crowd', 'Vel_reactive_neighbors/Vel_reactive_crowd', 'Proximity']
    # labels=['T/Tcr' , 'L/Lcr', 'J/Jcr', 'Reactivity\t\t', 'Neighbors\t\t', 'Prox', '\nColliding']
    # markers = [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2]
    labels=['T/Tcr' , 'L/Lcr', 'J/Jcr', 'NBR reac.', 'NBR vel.', 'Prox', 'Colliding']
    markers = [0, 0.2, 0.4, 0.6, 0.8, 1]
    str_markers = map(str, markers)

    #Path efficiency
    # (Talone/Tcrowded , Lalone/Lcrowded, Jalone/Jcrowded)
    path_efficiency = np.array([])
    # effect on crowd
    # (Vel_not_reactive_crowd/Vel_reactive_crowd, Vel_reactive_neighbors/Vel_reactive_crowd)
    effect_on_crowd = np.array([])
    # Proximity
    Proximity = np.array([])   
    # Collision
    Colliding = np.array([])

    # labels = np.delete(labels,3)

    for nav in robot_nav:
        for crowd_sim in directories:
            for scenario in range(1,21):
                if scenario in [2,3,4,6,7,8,10,11,12,14,15,16,18,19,20]:
                    continue
                for reac in reactivity:
                    # if reac == reactivity[1]:
                    #     continue
                    scenar = crowd_sim + nav + reac 
                    robot_alone_json = scenar+'0'+extension

                    path_efficiency = np.append(path_efficiency, metrics.compute_path_efficiency(robot_alone_json,scenar+str(scenario)+extension))
                    Proximity = np.append(Proximity, metrics.compute_crowd_robot_interaction(scenar+str(scenario)+extension))
                    Colliding = np.append(Colliding, metrics.get_time_spent_colliding(scenar+str(scenario)+extension))
                
                effect_on_crowd = np.append(effect_on_crowd, metrics.compute_effect_on_crowd(crowd_sim + nav + reactivity[1] + str(scenario) + extension, 
                crowd_sim + nav + reactivity[0] + str(scenario) + extension))

        path_efficiency = np.reshape(path_efficiency, (-1,3))
        effect_on_crowd = np.reshape(effect_on_crowd, (-1,2))

        to_plot = np.append(np.append(np.append(np.mean(path_efficiency, axis=0), np.mean(effect_on_crowd, axis=0)), np.mean(Proximity)), np.mean(Colliding))
        to_plot = np.append(np.append(np.append(np.std(path_efficiency, axis=0), np.std(effect_on_crowd, axis=0)), np.std(Proximity)), np.std(Colliding))
        # to_plot = np.delete(to_plot, 3)

        name = nav + " - crowd: " + str(crowd_sim[30:]).replace('/','')

        print(name + " " + str(to_plot.tolist()))

        # metrics.make_radar_chart(name, to_plot, labels, markers, str_markers)

        #Path efficiency
        # (Talone/Tcrowded , Lalone/Lcrowded, Jalone/Jcrowded)
        path_efficiency = np.array([])
        # effect on crowd
        # (Vel_not_reactive_crowd/Vel_reactive_crowd, Vel_reactive_neighbors/Vel_reactive_crowd)
        effect_on_crowd = np.array([])
        # Proximity
        Proximity = np.array([])   
        Colliding = np.array([])

    return

def test():

    # print(metrics.get_time_spent_colliding("../crowdbotsimcontrol/recorder/ORCA0_5s/BASELINE_notreactive_20.json"))
    # return
# all files
# BASELINE - crowd: ORCA0_5s [0.89565063 0.99997132 0.10271729 0.9776566  0.63898838 0.8933365 ]
# BASELINE - crowd: ORCA1_5s [0.90713404 1.         0.16693454 0.95671378 0.6158658  0.89093106]
# BASELINE - crowd: SocialForces [0.76683654 1.         0.01547457 0.97740224 0.68897134 0.89042699]
# DWA - crowd: ORCA0_5s [0.59666716 0.66716246 0.42283993 0.92278524 0.73215666 0.92325394]
# DWA - crowd: ORCA1_5s [0.58223989 0.6658303  0.38568141 0.87217161 0.74194351 0.9141248 ]
# DWA - crowd: SocialForces [0.58905992 0.72334545 0.3573741  0.92381555 0.74434513 0.92220439]
# RVO - crowd: ORCA0_5s [0.52922439 0.90979548 0.00470515 0.91937197 0.80086457 0.92252299]
# RVO - crowd: ORCA1_5s [4.91545731e-01 9.10986021e-01 6.23951706e-04 8.89128423e-01
#  8.25614337e-01 9.34902238e-01]
# RVO - crowd: SocialForces [4.96171737e-01 9.25926152e-01 7.77515957e-04 9.26296916e-01
#  8.27669271e-01 9.48550236e-01]

# All files, low density
# BASELINE - crowd (0.1pm2): ORCA0_5s [0.97289146 1.         0.10192884 0.97894978 0.39048876 0.97128525]
# BASELINE - crowd (0.1pm2): ORCA1_5s [0.98212864 1.         0.1328344  0.96270517 0.39040403 0.9787831 ]
# BASELINE - crowd (0.1pm2): SocialForces [0.96791293 1.         0.03202981 0.97967184 0.39857587 0.97456634]
# DWA - crowd (0.1pm2): ORCA0_5s [0.82056415 0.85632387 0.54298835 0.94278142 0.44855088 0.98145006]
# DWA - crowd (0.1pm2): ORCA1_5s [0.83174649 0.86727084 0.60096111 0.8955511  0.43185475 0.97691713]
# DWA - crowd (0.1pm2): SocialForces [0.79804554 0.88926978 0.54019755 0.91462225 0.45313927 0.96736961]
# RVO - crowd (0.1pm2): ORCA0_5s [0.80303757 0.97399628 0.01704699 0.96232145 0.47026513 0.97556665]
# RVO - crowd (0.1pm2): ORCA1_5s [0.7187046  0.94381065 0.00106474 0.97400119 0.53315568 0.97408257]
# RVO - crowd (0.1pm2): SocialForces [0.75218053 0.96038346 0.00119965 0.97276154 0.52190814 0.98169557]

# reactive only
# BASELINE - crowd: ORCA0_5s [0.9546731  0.99994264 0.18455698 0.9776566  0.48869352 1.        ]
# BASELINE - crowd: ORCA1_5s [0.98583523 1.         0.30656729 0.95671378 0.43853688 1.        ]
# BASELINE - crowd: SocialForces [0.8061712  1.         0.01360457 0.97740224 0.60947259 1.        ]
# DWA - crowd: ORCA0_5s [0.66155459 0.72535741 0.4692015  0.92278524 0.67360199 1.        ]
# DWA - crowd: ORCA1_5s [0.65001301 0.74369738 0.44597372 0.87217161 0.66847464 1.        ]
# DWA - crowd: SocialForces [0.65562788 0.74223539 0.40899689 0.92381555 0.68720012 1.        ]
# RVO - crowd: ORCA0_5s [0.51188662 0.91608419 0.00877189 0.91937197 0.75590963 1.        ]
# RVO - crowd: ORCA1_5s [5.08044067e-01 9.55307679e-01 7.38469909e-04 8.89128423e-01
#  7.51821257e-01 1.00000000e+00]
# RVO - crowd: SocialForces [4.85610061e-01 9.17181166e-01 9.01296900e-04 9.26296916e-01
#  7.94317407e-01 1.00000000e+00]

# LOW density, reactive only
# BASELINE - crowd: ORCA0_5s [0.98509848 1.         0.19825304 0.97894978 0.35840899 1.        ]
# BASELINE - crowd: ORCA1_5s [0.99565217 1.         0.23806551 0.96270517 0.33901143 1.        ]
# BASELINE - crowd: SocialForces [0.97254673 1.         0.0244593  0.97967184 0.37872846 1.        ]
# DWA - crowd: ORCA0_5s [0.87699127 0.91819499 0.52038293 0.94278142 0.3669646  1.        ]
# DWA - crowd: ORCA1_5s [0.84073776 0.87908696 0.52519272 0.8955511  0.36981113 1.        ]
# DWA - crowd: SocialForces [0.88796893 0.92097436 0.64305743 0.91462225 0.36582839 1.        ]
# RVO - crowd: ORCA0_5s [0.80246746 0.97195966 0.03308744 0.96232145 0.38900217 1.        ]
# RVO - crowd: ORCA1_5s [0.78075317 0.97327207 0.00128459 0.97400119 0.41757378 1.        ]
# RVO - crowd: SocialForces [0.74346819 0.956416   0.00147681 0.97276154 0.4913544  1.        ]

    labels=['T/Tcr' , 'L/Lcr', 'J/Jcr', 'NBR reac.', 'NBR vel.', 'Prox', 'Colliding']
    markers = [0, 0.2, 0.4, 0.6, 0.8, 1]
    str_markers = map(str, markers)
    dico = {
        'BASELINE - crowd: ORCA0_5s': [0.8956506257676893, 0.9999713195500911, 0.1027172939809377, 0.7023804399987416, 0.9434892067519609, 0.8077174396113369, 0.8933365017072056],
        'BASELINE - crowd: ORCA1_5s': [0.907134040435295, 1.0, 0.16693453927347557, 0.8405503380134617, 0.9277083595580093, 0.7676458596488329, 0.8909310554219022],
        'BASELINE - crowd: SocialForces': [0.7668365402362294, 1.0, 0.015474571640059146, 0.7935663972799445, 0.9109325773525423, 0.7883948655987374, 0.8904269939647446],
        'DWA - crowd: ORCA0_5s': [0.59666716013337, 0.6671624624367858, 0.42283992974113344, 0.4138044056043785, 0.8570759534508966, 0.770161954165741, 0.923253936208523],
        'DWA - crowd: ORCA1_5s': [0.5822398868801015, 0.6658303046112632, 0.3856814097296194, 0.4493211754429609, 0.8147376464127211, 0.6997511593538891, 0.9141247983117875],
        'DWA - crowd: SocialForces': [0.5890599237266038, 0.7233454484273796, 0.3573740960805869, 0.3738063865745342, 0.8385062912922571, 0.6846154167643871, 0.9222043947633608],
        'RVO - crowd: ORCA0_5s': [0.5292243893144409, 0.9097954847718196, 0.004705150646644595, 0.5072505125915108, 0.8021608973705916, 0.7483259075958155, 0.9225229935967377],
        'RVO - crowd: ORCA1_5s': [0.4915457311866779, 0.910986021250273, 0.0006239517061351875, 0.9351643110644483, 0.799613168876297, 0.7369435835489095, 0.9349022378942486],
        'RVO - crowd: SocialForces': [0.4961717373725751, 0.9259261519811346, 0.0007775159567548075, 0.5557350869798076, 0.7903266358149278, 0.7608806514414057, 0.9485502363018832]
    }

    dico2 = { 
        'DWA' : np.mean( np.array([dico['DWA - crowd: ORCA0_5s'], dico['DWA - crowd: ORCA1_5s'], dico['DWA - crowd: SocialForces']]) ,axis=0).tolist(),
        'BASELINE' : np.mean( np.array([dico['BASELINE - crowd: ORCA0_5s'], dico['BASELINE - crowd: ORCA1_5s'], dico['BASELINE - crowd: SocialForces']]) ,axis=0).tolist(),
        'RVO' : np.mean( np.array([dico['RVO - crowd: ORCA0_5s'], dico['RVO - crowd: ORCA1_5s'], dico['RVO - crowd: SocialForces']]) ,axis=0).tolist() 
    }

    # labels.pop(3)
    # for key, value in dico2.items():
    #     value.pop(3)

    metrics.make_radar_chart2('High density\n', dico2, labels, markers, str_markers)

    
    # All files, low density
    dico3 = {
        'DWA' : np.mean(np.array([[0.8205641480698708, 0.8563238694774504, 0.5429883500743731, 0.5876872911189577, 0.8872052310385328, 0.6783621131272051, 0.9814500551475985],[0.8317464909735193, 0.8672708396209059, 0.6009611075122457, 0.7378530938949754, 0.8634411546357417, 0.5728794638691974, 0.9769171250486186],[0.798045537443947, 0.8892697839738684, 0.5401975529650659, 0.37456315655679956, 0.8445750305513574, 0.5474936905801291, 0.9673696068563457]])  ,axis=0).tolist(),
        'BASELINE' : np.mean(np.array([[0.9728914573193131, 1.0, 0.10192883690201897, 0.7752264604891403, 0.9347462811041053, 0.6728259087695265, 0.9712852510626464],[0.9821286427494946, 1.0, 0.13283440125885676, 1.1461485446434152, 0.9321251228628219, 0.6441936791445027, 0.9787830992464845],[0.967912931886562, 1.0, 0.032029814359136995, 0.9916028665350206, 0.9196958210623529, 0.6647598785027731, 0.9745663361324981]])  ,axis=0).tolist(),
        'RVO' : np.mean(np.array([[0.8030375714105566, 0.9739962802730021, 0.017046994894939, 1, 0.8695366453484684, 0.6541818440335729, 0.9755666494729324],[0.7187046033212897, 0.9438106523488571, 0.0010647363339008305, 1.0, 0.9219734050098459, 0.5697100903470158, 0.9740825676702171],[0.7521805336302376, 0.9603834574970591, 0.0011996464155804075, 1, 0.8566749023796587, 0.6889661704831871, 0.9816955700274284]])  ,axis=0).tolist(),
    }
    # BASELINE - crowd (0.1pm2): ORCA0_5s [0.97289146 1.         0.10192884 0.97894978 0.39048876 0.97128525]
    # BASELINE - crowd (0.1pm2): ORCA1_5s [0.98212864 1.         0.1328344  0.96270517 0.39040403 0.9787831 ]
    # BASELINE - crowd (0.1pm2): SocialForces [0.96791293 1.         0.03202981 0.97967184 0.39857587 0.97456634]
    # DWA - crowd (0.1pm2): ORCA0_5s [0.82056415 0.85632387 0.54298835 0.94278142 0.44855088 0.98145006]
    # DWA - crowd (0.1pm2): ORCA1_5s [0.83174649 0.86727084 0.60096111 0.8955511  0.43185475 0.97691713]
    # DWA - crowd (0.1pm2): SocialForces [0.79804554 0.88926978 0.54019755 0.91462225 0.45313927 0.96736961]
    # RVO - crowd (0.1pm2): ORCA0_5s [0.80303757 0.97399628 0.01704699 0.96232145 0.47026513 0.97556665]
    # RVO - crowd (0.1pm2): ORCA1_5s [0.7187046  0.94381065 0.00106474 0.97400119 0.53315568 0.97408257]
    # RVO - crowd (0.1pm2): SocialForces [0.75218053 0.96038346 0.00119965 0.97276154 0.52190814 0.98169557]

    
    metrics.make_radar_chart2('Low density\n', dico3, labels, markers, str_markers)

    return

if __name__ == "__main__":

    try:
        option = argv[1]
    except IndexError:
        option = ''
    
    if option=='':
        run(HOST='127.0.0.1', PORT=25001, time_step=0.1, sleep_time=-0.5, planner_type="RVO", crowd_is_reactive=True, crowd_simulator='SocialForces')

    if option=='-m':
        compute_metrics()

    if option=='-t':
        test()

    if option=='-d':
        demo_run()