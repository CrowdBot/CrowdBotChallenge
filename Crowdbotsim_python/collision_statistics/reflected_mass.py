import numpy as np

class Pendulum:
	def __init__(self, mass, length, moment_of_inertia, name=None):
		self.m = mass
		self.l = length
		self.t = moment_of_inertia
		self.name = name

def computeJacobian(sub_chain):
	dof = 3+len(sub_chain)-1
	J = np.concatenate((np.eye(2), np.zeros([2, dof-2])), axis=1)
	J[1, dof-1] = sub_chain[-1].l/2
	for j in range(1, dof-3):
		J[1, dof-1-j] = J[1, dof-j]+sub_chain[-1-j].l
	J[1, 2] = J[1, 3]+sub_chain[0].l/2
	return J

def computeReflectedMass(pendula, target_pendulum_index):
	dof = 3+len(pendula)-1
	mass_matrix = np.zeros([dof, dof])
	# q = [x_0 y_0 phi_0 alpha_1 alpha_2 ... alpha_-1 alpha_-2 ...]

	J_trans_0 = np.concatenate((np.eye(2), np.zeros([2, dof-2])), axis=1)
	m_0 = pendula[target_pendulum_index].m
	mass_matrix += m_0*np.matmul(np.transpose(J_trans_0), J_trans_0)

	J_rot_0 = np.concatenate((np.zeros([1,2]), np.eye(1), np.zeros([1, dof-3])), axis=1)
	t_0 = pendula[target_pendulum_index].t
	mass_matrix += t_0*np.matmul(np.transpose(J_rot_0), J_rot_0)

	for j in range(target_pendulum_index + 1, len(pendula)):
		J_trans_sub = computeJacobian(pendula[target_pendulum_index:(j+1)])
		J_trans = np.concatenate((np.eye(2), np.zeros([2, dof-2])), axis=1)
		J_trans[:, 2:(3+j-target_pendulum_index)] = J_trans_sub[:, 2:]
		mass_matrix += pendula[j].m*np.matmul(np.transpose(J_trans), J_trans)
		J_rot = np.concatenate((np.zeros([1,2]),
			np.ones([1, 1+j-target_pendulum_index]),
			np.zeros([1, target_pendulum_index+len(pendula)-j-1])), axis=1)
		#print (mass_matrix.shape, J_rot.shape)
		mass_matrix += pendula[j].t*np.matmul(np.transpose(J_rot), J_rot)

	for j in range(0, target_pendulum_index):
		sub_chain = pendula[j:target_pendulum_index+1]
		sub_chain.reverse()
		J_trans_sub = computeJacobian(sub_chain)
		J_trans = np.concatenate((np.eye(2), np.zeros([2, dof-2])), axis=1)
		J_trans[:, 2] = -J_trans_sub[:, 2]
		J_trans[:, (2+len(pendula)-target_pendulum_index):(dof-j)] = -J_trans_sub[:, 3:]
		mass_matrix += pendula[j].m*np.matmul(np.transpose(J_trans), J_trans)
		J_rot = np.concatenate((np.zeros([1,2]), np.eye(1),
			np.zeros([1, len(pendula)-target_pendulum_index-1]),
			np.ones([1, target_pendulum_index-j]), np.zeros([1, j])), axis=1)
		mass_matrix += pendula[j].t*np.matmul(np.transpose(J_rot), J_rot)

	inverse_mass_matrix = np.linalg.inv(mass_matrix)
	reflected_mass = 1.0/inverse_mass_matrix[1,1]
	return reflected_mass

def cylinder(m, l, r, name):
	theta = m*(1.0/4.0*r*r+1.0/12.0*l*l)
	return Pendulum(m, l, theta, name)

def main():
	total_body_mass = 70.0
	total_body_height = 1.75
	
	feet_mass_fraction = 2*0.0145
	lower_legs_mass_fraction = 2*0.0465
	thighs_mass_fraction = 2*0.1
	head_arms_trunk_mass_fraction = 0.678

	feet_height_fraction = 0.039
	lower_legs_height_fraction = 0.285-0.039
	thighs_height_fraction = 0.53-0.285
	upper_body_height_fraction = 1.0-0.53

	foot = cylinder(total_body_mass*feet_mass_fraction,
		total_body_height*feet_height_fraction, 0.25, "feet")
	calf = cylinder(total_body_mass*lower_legs_mass_fraction,
		total_body_height*lower_legs_height_fraction, 0.1, "calfs")
	thigh = cylinder(total_body_mass*thighs_mass_fraction,
		total_body_height*thighs_height_fraction, 0.2, "thighs")
	upper_body = cylinder(total_body_mass*head_arms_trunk_mass_fraction,
		total_body_height*upper_body_height_fraction, 0.22, "head + arms + trunk")
	pendula = [foot, calf, thigh, upper_body]
	for i in range(len(pendula)):
		m_refl = computeReflectedMass(pendula, i)
		print "%s: %f kg (reflected), %f kg (single)"%(pendula[i].name, m_refl, pendula[i].m)

if __name__ == '__main__':
	main()