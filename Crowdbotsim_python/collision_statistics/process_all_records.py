from parse_record import SampleCollector
import matplotlib.pyplot as plt
import cPickle as pickle

def save_object(obj, filename):
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

folder_ORCA0_5s = "../crowdbotsimcontrol/recorder/ORCA0_5s/"
folder_ORCA1_5s = "../crowdbotsimcontrol/recorder/ORCA1_5s/"
folder_SocialForces = "../crowdbotsimcontrol/recorder/SocialForces/"

filenames_per_method_ORCA0_5s = [
"""BASELINE_notreactive_0.json
BASELINE_notreactive_10.json
BASELINE_notreactive_11.json
BASELINE_notreactive_12.json
BASELINE_notreactive_13.json
BASELINE_notreactive_14.json
BASELINE_notreactive_15.json
BASELINE_notreactive_16.json
BASELINE_notreactive_17.json
BASELINE_notreactive_18.json
BASELINE_notreactive_19.json
BASELINE_notreactive_1.json
BASELINE_notreactive_20.json
BASELINE_notreactive_2.json
BASELINE_notreactive_3.json
BASELINE_notreactive_4.json
BASELINE_notreactive_5.json
BASELINE_notreactive_6.json
BASELINE_notreactive_7.json
BASELINE_notreactive_8.json
BASELINE_notreactive_9.json
BASELINE_reactive_0.json
BASELINE_reactive_10.json
BASELINE_reactive_11.json
BASELINE_reactive_12.json
BASELINE_reactive_13.json
BASELINE_reactive_14.json
BASELINE_reactive_15.json
BASELINE_reactive_16.json
BASELINE_reactive_17.json
BASELINE_reactive_18.json
BASELINE_reactive_19.json
BASELINE_reactive_1.json
BASELINE_reactive_20.json
BASELINE_reactive_2.json
BASELINE_reactive_3.json
BASELINE_reactive_4.json
BASELINE_reactive_5.json
BASELINE_reactive_6.json
BASELINE_reactive_7.json
BASELINE_reactive_8.json
BASELINE_reactive_9.json""",
"""DWA_notreactive_0.json
DWA_notreactive_10.json
DWA_notreactive_11.json
DWA_notreactive_12.json
DWA_notreactive_13.json
DWA_notreactive_14.json
DWA_notreactive_15.json
DWA_notreactive_16.json
DWA_notreactive_17.json
DWA_notreactive_18.json
DWA_notreactive_19.json
DWA_notreactive_1.json
DWA_notreactive_20.json
DWA_notreactive_2.json
DWA_notreactive_3.json
DWA_notreactive_4.json
DWA_notreactive_5.json
DWA_notreactive_6.json
DWA_notreactive_7.json
DWA_notreactive_8.json
DWA_notreactive_9.json
DWA_reactive_0.json
DWA_reactive_10.json
DWA_reactive_11.json
DWA_reactive_12.json
DWA_reactive_13.json
DWA_reactive_14.json
DWA_reactive_15.json
DWA_reactive_16.json
DWA_reactive_17.json
DWA_reactive_18.json
DWA_reactive_19.json
DWA_reactive_1.json
DWA_reactive_20.json
DWA_reactive_2.json
DWA_reactive_3.json
DWA_reactive_4.json
DWA_reactive_5.json
DWA_reactive_6.json
DWA_reactive_7.json
DWA_reactive_8.json
DWA_reactive_9.json""",
"""RVO_notreactive_0.json
RVO_notreactive_10.json
RVO_notreactive_11.json
RVO_notreactive_12.json
RVO_notreactive_13.json
RVO_notreactive_14.json
RVO_notreactive_15.json
RVO_notreactive_16.json
RVO_notreactive_17.json
RVO_notreactive_18.json
RVO_notreactive_19.json
RVO_notreactive_1.json
RVO_notreactive_20.json
RVO_notreactive_2.json
RVO_notreactive_3.json
RVO_notreactive_4.json
RVO_notreactive_5.json
RVO_notreactive_6.json
RVO_notreactive_7.json
RVO_notreactive_8.json
RVO_notreactive_9.json
RVO_reactive_0.json
RVO_reactive_10.json
RVO_reactive_11.json
RVO_reactive_12.json
RVO_reactive_13.json
RVO_reactive_14.json
RVO_reactive_15.json
RVO_reactive_16.json
RVO_reactive_17.json
RVO_reactive_18.json
RVO_reactive_19.json
RVO_reactive_1.json
RVO_reactive_20.json
RVO_reactive_2.json
RVO_reactive_3.json
RVO_reactive_4.json
RVO_reactive_5.json
RVO_reactive_6.json
RVO_reactive_7.json
RVO_reactive_8.json
RVO_reactive_9.json"""
]

filenames_per_method_ORCA1_5s = [
"""BASELINE_notreactive_0.json
BASELINE_notreactive_10.json
BASELINE_notreactive_11.json
BASELINE_notreactive_12.json
BASELINE_notreactive_13.json
BASELINE_notreactive_14.json
BASELINE_notreactive_15.json
BASELINE_notreactive_16.json
BASELINE_notreactive_17.json
BASELINE_notreactive_18.json
BASELINE_notreactive_19.json
BASELINE_notreactive_1.json
BASELINE_notreactive_20.json
BASELINE_notreactive_2.json
BASELINE_notreactive_3.json
BASELINE_notreactive_4.json
BASELINE_notreactive_5.json
BASELINE_notreactive_6.json
BASELINE_notreactive_7.json
BASELINE_notreactive_8.json
BASELINE_notreactive_9.json
BASELINE_reactive_0.json
BASELINE_reactive_10.json
BASELINE_reactive_11.json
BASELINE_reactive_12.json
BASELINE_reactive_13.json
BASELINE_reactive_14.json
BASELINE_reactive_15.json
BASELINE_reactive_16.json
BASELINE_reactive_17.json
BASELINE_reactive_18.json
BASELINE_reactive_19.json
BASELINE_reactive_1.json
BASELINE_reactive_20.json
BASELINE_reactive_2.json
BASELINE_reactive_3.json
BASELINE_reactive_4.json
BASELINE_reactive_5.json
BASELINE_reactive_6.json
BASELINE_reactive_7.json
BASELINE_reactive_8.json
BASELINE_reactive_9.json""",
"""DWA_notreactive_0.json
DWA_notreactive_10.json
DWA_notreactive_11.json
DWA_notreactive_12.json
DWA_notreactive_13.json
DWA_notreactive_14.json
DWA_notreactive_15.json
DWA_notreactive_16.json
DWA_notreactive_17.json
DWA_notreactive_18.json
DWA_notreactive_19.json
DWA_notreactive_1.json
DWA_notreactive_20.json
DWA_notreactive_2.json
DWA_notreactive_3.json
DWA_notreactive_4.json
DWA_notreactive_5.json
DWA_notreactive_6.json
DWA_notreactive_7.json
DWA_notreactive_8.json
DWA_notreactive_9.json
DWA_reactive_0.json
DWA_reactive_10.json
DWA_reactive_11.json
DWA_reactive_12.json
DWA_reactive_13.json
DWA_reactive_14.json
DWA_reactive_15.json
DWA_reactive_16.json
DWA_reactive_17.json
DWA_reactive_18.json
DWA_reactive_19.json
DWA_reactive_1.json
DWA_reactive_20.json
DWA_reactive_2.json
DWA_reactive_3.json
DWA_reactive_4.json
DWA_reactive_5.json
DWA_reactive_6.json
DWA_reactive_7.json
DWA_reactive_8.json
DWA_reactive_9.json""",
"""RVO_notreactive_0.json
RVO_notreactive_10.json
RVO_notreactive_11.json
RVO_notreactive_12.json
RVO_notreactive_13.json
RVO_notreactive_14.json
RVO_notreactive_15.json
RVO_notreactive_16.json
RVO_notreactive_17.json
RVO_notreactive_18.json
RVO_notreactive_19.json
RVO_notreactive_1.json
RVO_notreactive_20.json
RVO_notreactive_2.json
RVO_notreactive_3.json
RVO_notreactive_4.json
RVO_notreactive_5.json
RVO_notreactive_6.json
RVO_notreactive_7.json
RVO_notreactive_8.json
RVO_notreactive_9.json
RVO_reactive_0.json
RVO_reactive_10.json
RVO_reactive_11.json
RVO_reactive_12.json
RVO_reactive_13.json
RVO_reactive_14.json
RVO_reactive_15.json
RVO_reactive_16.json
RVO_reactive_17.json
RVO_reactive_18.json
RVO_reactive_19.json
RVO_reactive_1.json
RVO_reactive_20.json
RVO_reactive_2.json
RVO_reactive_3.json
RVO_reactive_4.json
RVO_reactive_5.json
RVO_reactive_6.json
RVO_reactive_7.json
RVO_reactive_8.json
RVO_reactive_9.json"""
]

filenames_per_method_SocialForces = [
"""BASELINE_notreactive_0.json
BASELINE_notreactive_10.json
BASELINE_notreactive_11.json
BASELINE_notreactive_12.json
BASELINE_notreactive_13.json
BASELINE_notreactive_14.json
BASELINE_notreactive_15.json
BASELINE_notreactive_16.json
BASELINE_notreactive_17.json
BASELINE_notreactive_18.json
BASELINE_notreactive_19.json
BASELINE_notreactive_1.json
BASELINE_notreactive_20.json
BASELINE_notreactive_2.json
BASELINE_notreactive_3.json
BASELINE_notreactive_4.json
BASELINE_notreactive_5.json
BASELINE_notreactive_6.json
BASELINE_notreactive_7.json
BASELINE_notreactive_8.json
BASELINE_notreactive_9.json
BASELINE_reactive_0.json
BASELINE_reactive_10.json
BASELINE_reactive_11.json
BASELINE_reactive_12.json
BASELINE_reactive_13.json
BASELINE_reactive_14.json
BASELINE_reactive_15.json
BASELINE_reactive_16.json
BASELINE_reactive_17.json
BASELINE_reactive_18.json
BASELINE_reactive_19.json
BASELINE_reactive_1.json
BASELINE_reactive_20.json
BASELINE_reactive_2.json
BASELINE_reactive_3.json
BASELINE_reactive_4.json
BASELINE_reactive_5.json
BASELINE_reactive_6.json
BASELINE_reactive_7.json
BASELINE_reactive_8.json
BASELINE_reactive_9.json""",
"""DWA_notreactive_0.json
DWA_notreactive_10.json
DWA_notreactive_11.json
DWA_notreactive_12.json
DWA_notreactive_13.json
DWA_notreactive_14.json
DWA_notreactive_15.json
DWA_notreactive_16.json
DWA_notreactive_17.json
DWA_notreactive_18.json
DWA_notreactive_19.json
DWA_notreactive_1.json
DWA_notreactive_20.json
DWA_notreactive_2.json
DWA_notreactive_3.json
DWA_notreactive_4.json
DWA_notreactive_5.json
DWA_notreactive_6.json
DWA_notreactive_7.json
DWA_notreactive_8.json
DWA_notreactive_9.json
DWA_reactive_0.json
DWA_reactive_10.json
DWA_reactive_11.json
DWA_reactive_12.json
DWA_reactive_13.json
DWA_reactive_14.json
DWA_reactive_15.json
DWA_reactive_16.json
DWA_reactive_17.json
DWA_reactive_18.json
DWA_reactive_19.json
DWA_reactive_1.json
DWA_reactive_20.json
DWA_reactive_2.json
DWA_reactive_3.json
DWA_reactive_4.json
DWA_reactive_5.json
DWA_reactive_6.json
DWA_reactive_7.json
DWA_reactive_8.json
DWA_reactive_9.json""",
"""RVO_notreactive_0.json
RVO_notreactive_10.json
RVO_notreactive_11.json
RVO_notreactive_12.json
RVO_notreactive_13.json
RVO_notreactive_14.json
RVO_notreactive_15.json
RVO_notreactive_16.json
RVO_notreactive_17.json
RVO_notreactive_18.json
RVO_notreactive_19.json
RVO_notreactive_1.json
RVO_notreactive_20.json
RVO_notreactive_2.json
RVO_notreactive_3.json
RVO_notreactive_4.json
RVO_notreactive_5.json
RVO_notreactive_6.json
RVO_notreactive_7.json
RVO_notreactive_8.json
RVO_notreactive_9.json
RVO_reactive_0.json
RVO_reactive_10.json
RVO_reactive_11.json
RVO_reactive_12.json
RVO_reactive_13.json
RVO_reactive_14.json
RVO_reactive_15.json
RVO_reactive_16.json
RVO_reactive_17.json
RVO_reactive_18.json
RVO_reactive_19.json
RVO_reactive_1.json
RVO_reactive_20.json
RVO_reactive_2.json
RVO_reactive_3.json
RVO_reactive_4.json
RVO_reactive_5.json
RVO_reactive_6.json
RVO_reactive_7.json
RVO_reactive_8.json
RVO_reactive_9.json"""
]

all_folders = [folder_ORCA0_5s , folder_ORCA1_5s, folder_SocialForces]
all_filenames_per_method = [filenames_per_method_ORCA0_5s,
	filenames_per_method_ORCA1_5s, filenames_per_method_SocialForces]
output_names = ["ORCA0_5s", "ORCA1_5s", "SocialForces"]

no_processing = True
sim_time_sums = [0.0, 0.0, 0.0]

for s in range(3):
	folder = all_folders[s]
	filenames_per_method = all_filenames_per_method[s]

	body_parts_keys = ["Toe0", "Calf", "Thigh", "UpperArm"]

	samples_per_method_and_body_part = [
		[
			[] for i in range(4)
		] for m in range(3)
	]

	for m in range(3):
		filenames = filenames_per_method[m].split()
		for n in range(len(filenames)):
			filepath = folder + filenames[n]
			print (filepath)
			sc = SampleCollector(filepath, no_processing)
			sim_time_sums[m] += float(sc.json_data[-1]["clock"])
			if no_processing:
				continue
			#sc.plotForm()
			for i in range(4):
				for j in range(len(sc.form[body_parts_keys[i]])):
					for k in range(len(sc.form[body_parts_keys[i]][j][0])):
						speed = sc.form[body_parts_keys[i]][j][0][k]
						samples_per_method_and_body_part[m][i].append(speed)

	if no_processing:
		print "NO SAVING - NO PROCESSING"
		continue
	save_object(samples_per_method_and_body_part,
		'samples'+output_names[s]+'.pkl')

print "Total simulation times: Baseline, DWA, RVO"
print sim_time_sums